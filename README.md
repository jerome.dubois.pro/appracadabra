# Notes

- Tweets could be streamed from twitter
- Only 100 Tweets are retrieved fro mrecent search. Pagination could be used to get all.
- As of now, Interfaces need to be more properly defined. They are embedded in implementation classes.
- persistence adapter does too much (intelligence), .ie for removal
- API for timer should be defined
- for tweet_ingestion, API timer will call should be defined (like notify_suggestion(command_id (purge, refresh)))
- API for user should be defined: set refresh intervals for update or purge
- tweets_ingestion should call back timer to set next update
