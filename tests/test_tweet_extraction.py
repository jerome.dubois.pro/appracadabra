import os
from appracadabra.adapters.implementation_adapters import PandasPersistenceAdapter
import tweepy  # type: ignore
from appracadabra.domain_logic.tweets_ingestion import (
    extract_tweet_data_from_response,
    get_response_include_media_video_list,
    TweetDataIngester,
)


def test_video_single_tweet():
    bearer_token = os.getenv("BEARER_TOKEN")
    if not bearer_token:
        print(
            "Please set BEARER_TOKEN env variable, like BEARER_TOKEN='eeeeeerererere' python application.py"
        )
    client = tweepy.Client(bearer_token)
    # This is a tweet from nvidia with a video
    response = client.get_tweet(
        1517550072304652289,
        expansions="attachments.media_keys",
        tweet_fields="public_metrics",
        media_fields="public_metrics",
    )
    response_include_media_video_list = get_response_include_media_video_list(response)
    tweet_data = extract_tweet_data_from_response(
        response.data, response_include_media_video_list
    )
    print(tweet_data)
    assert "The GeForce RTX Keycap" in tweet_data.text
    assert tweet_data.id == 1517550072304652289
    assert tweet_data.reply_count >= 174
    assert tweet_data.like_count >= 3652
    assert tweet_data.retweet_count >= 3322
    assert tweet_data.media_info is not None
    assert tweet_data.media_info.media_key == "13_1517550047352827905"
    assert tweet_data.media_info.video_view_count >= 53419
    # print(tweet_data)


def test_search_recent_tweets_from_hashtag():
    panda_db_file_name = "TestSearchRecentTweetsFromHashtag.csv"
    if os.path.exists(panda_db_file_name):
        os.remove(panda_db_file_name)
    bearer_token = os.getenv("BEARER_TOKEN")
    if not bearer_token:
        print(
            "Please set BEARER_TOKEN env variable, like BEARER_TOKEN='eeeeeerererere' python application.py"
        )
    tweepy_client = tweepy.Client(bearer_token)
    persistence_adapter = PandasPersistenceAdapter()
    persistence_adapter.init_db("TestSearchRecentTweetsFromHashtag.csv")
    tweet_data_ingester = TweetDataIngester(
        client=tweepy_client, persistence_adapter=persistence_adapter
    )
    # We test for 10 results only for speed sake
    tweet_data_list = tweet_data_ingester.get_tweet_data_list_from_hashtag(
        max_results=10
    )
    # TODO: DISCUSSION: Are there always arcane data in recent tweets...? Could be mocked.
    # assert len(tweet_data_list)
    for tweet_data in tweet_data_list:
        # We must have a creation date
        assert tweet_data.created_at
        # We must find #[a,A]rcane in text field if it is not a reference to another tweet
        # Else We would have to go fetch other tweet fulltext to see if arcane really present
        # TODO: DISCUSSION: There is an uncertainty all tweets are relevant here...
        if "#arcane" not in tweet_data.text.lower():
            if tweet_data.references_other_tweet:
                print(
                    "WARNING: Ignoring potential problem. Could not detect #arcane in tweet text, but it is a reference to other so maybe we need full text or referee"
                )
            else:
                assert False
