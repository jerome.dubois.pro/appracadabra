import datetime
import os
from typing import List
import tweepy  # type: ignore
from appracadabra.adapters.implementation_adapters import PandasPersistenceAdapter
from appracadabra.domain_logic.models import TweetData
from appracadabra.domain_logic.tweets_ingestion import (
    TweetDataIngester,
)

if __name__ == "__main__":

    # TODO: Tweets could be streamed to DB rather than sent in batches
    # TODO: only one page of 100 tweets is treated right now...
    # TODO: Tweets could be retrieved using streams to get instantaneous updates

    # Client can set a Timer setting for automatic update of tweets through tweet_ingetsion API
    # Which will register a timer to signal back tweet_ingestion API for update

    # Client can set a Timer setting for automatic purge of old tweet data
    # Which will register a timer to signal back tweet_ingestion API for purge

    bearer_token = os.getenv("BEARER_TOKEN")
    if not bearer_token:
        print(
            "Please set BEARER_TOKEN env variable, like BEARER_TOKEN='eeeeeerererere' python application.py"
        )
        import sys

        sys.exit(1)
    tweepy_client = tweepy.Client(bearer_token)
    persistence_adapter = PandasPersistenceAdapter()
    persistence_adapter.init_db("TweetDataDB.csv")
    twiter_data_ingester = TweetDataIngester(
        client=tweepy_client, persistence_adapter=persistence_adapter
    )

    # We poll for 1 day (86400 seconds) of data, this could happend through a Timer
    # TODO: Current implementation limit: only 100 last tweets + poll date filtering.
    # In practice, would need to filter last minutes/seconds to have impact here
    # with current implementation
    X = 86400
    poll_date_limit = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(
        seconds=X
    )
    poll_date_limit_str = str(poll_date_limit.strftime("%Y-%m-%dT%H:%M:%S+02:00"))

    # Time requires update
    twiter_data_ingester.get_tweet_data_list_from_hashtag(
        old_date_limite=poll_date_limit_str
    )

    # Timer requires purge
    # We choose to purge data older than 10 days
    X = 86400*10
    old_date_limit = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(
        seconds=X
    )
    old_date_limit_str = str(old_date_limit.strftime("%Y-%m-%d %H:%M:%S"))
    twiter_data_ingester.remove_old_tweet_data(old_date_limit_str)
    pass
