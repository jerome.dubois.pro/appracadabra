# Python Script to Extract tweets of a
# particular Hashtag using Tweepy and Pandas

# import modules


from dataclasses import dataclass, field
import datetime
from typing import List, Optional, Tuple
from appracadabra.adapters.interface_adapters import PersistenceAdapter, TimerAdapter
from numpy import int64
from appracadabra.adapters.implementation_adapters import DummyTimer, PandasPersistenceAdapter  # type: ignore
from appracadabra.domain_logic.models import TweetData
import tweepy  # type: ignore


@dataclass
class TweetDataIngester:
    """Class dealing with tweet data ingestion"""
    client: tweepy.Client # should be InterfaceTwitterAPIv2
    persistence_adapter: PersistenceAdapter
    timer_adapter: TimerAdapter = field(default=DummyTimer())

    def get_tweet_data_list_from_hashtag(
        self,
        hashtag="#Arcane",
        old_date_limite: Optional[datetime.datetime] = None,
        max_results=100,
    ) -> List[TweetData]:
        response = self.client.search_recent_tweets(
            hashtag,
            max_results=max_results,
            expansions=["attachments.media_keys", "referenced_tweets.id"],
            tweet_fields=["public_metrics", "created_at"],
            media_fields="public_metrics",
            start_time=old_date_limite,
        )
        tweet_data_list = get_tweet_data_list_from_reponse(response)
        indices, rows = extract_rows_from_tweet_data_list(tweet_data_list)
        self.persistence_adapter.update_db(indices, rows)
        self.timer_adapter.signal_next_update()
        return tweet_data_list

    def remove_old_tweet_data(self, old_time):
        self.persistence_adapter.persist_old_tweet_data_removal(old_time)


def extract_tweet_data_from_response(
    response_data: tweepy.tweet.Tweet,
    response_include_media_video_list: List[tweepy.media.Media],
) -> TweetData:
    """Generate TweetData object from Tweet and list of medias"""
    references_other_tweet = True if response_data.referenced_tweets else False

    id = response_data.id
    created_at = response_data.created_at
    text = response_data.text
    reply_count = response_data.public_metrics["reply_count"]
    like_count = response_data.public_metrics["like_count"]
    retweet_count = response_data.public_metrics["retweet_count"]
    tweet_data = TweetData(
        id,
        created_at,
        text,
        reply_count,
        like_count,
        retweet_count,
        references_other_tweet,
    )
    if response_data.attachments:
        tweet_data.add_media_info_from_response(
            response_data.attachments["media_keys"], response_include_media_video_list
        )
    return tweet_data


def get_response_include_media_video_list(response):
    """Extract video related data from response"""
    return [media for media in response.includes["media"] if media.type == "video"]


def get_tweet_data_list_from_reponse(response):
    """Return tweet data list from response"""
    tweet_data_list: List[TweetData] = []
    response_include_media_video_list = get_response_include_media_video_list(response)
    for response_data in response.data:
        tweet_data = extract_tweet_data_from_response(
            response_data, response_include_media_video_list
        )
        tweet_data_list.append(tweet_data)
        # print(tweet_data)
    return tweet_data_list


def extract_rows_from_tweet_data_list(
    tweet_data_list: List[TweetData],
) -> Tuple[
    List[int64], List[Tuple[datetime.datetime, str, int, int, int, bool, str, int]]
]:
    """Prepare data for DB persistence"""
    indices = [int64(tweet_data.id) for tweet_data in tweet_data_list]
    rows = [
        (
            tweet_data.created_at,
            tweet_data.text,
            tweet_data.reply_count,
            tweet_data.like_count,
            tweet_data.retweet_count,
            tweet_data.references_other_tweet,
            tweet_data.media_info.media_key if tweet_data.media_info else "-1",
            tweet_data.media_info.video_view_count if tweet_data.media_info else -1,
        )
        for tweet_data in tweet_data_list
    ]
    return indices, rows
