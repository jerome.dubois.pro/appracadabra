"""Module to provide models, classes holding data for this logic domain"""

from dataclasses import dataclass, field
import datetime
from typing import List, Optional

from numpy import int64
import tweepy  # type: ignore


@dataclass
class MediaInfo:
    """Class used to hold info related to media (video ATM)"""

    media_key: str
    video_view_count: int


@dataclass
class TweetData:
    """Representation of the relevant data fro a tweet we are interested in"""

    id: int64
    created_at: datetime.datetime
    text: str
    reply_count: int
    like_count: int
    retweet_count: int
    references_other_tweet: bool
    media_info: Optional[MediaInfo] = field(default=None)

    def add_media_info_from_response(
        self, media_keys: List, response_include_media_video_list: tweepy.media.Media
    ):
        """Search for videos and store relevant info for first encountered video."""
        # print("WHOLE MEDIA LIST", type(response_include_media_video_list), response_include_media_video_list)
        tweet_medias = [
            media
            for media in response_include_media_video_list
            if media.media_key in media_keys
        ]
        # TODO: Discussion: can there be multiple videos? Right now, only first one taken into account!!!
        for tweet_media in tweet_medias:
            # TODO: Discussion: we give the whole media list. Could be filtered beforehand to optimize treatment
            # Bu then we lose opprtunity to further process photos or other medias here
            if tweet_media.type == "video":
                self.media_info = MediaInfo(
                    tweet_media.media_key, tweet_media.public_metrics["view_count"]
                )
                break
