import abc
from datetime import datetime
from typing import List, Tuple
from numpy import int64


class InterfaceTwitterAPIv2(metaclass=abc.ABCMeta):
    """Interface for Twitter v2 API"""

    @classmethod
    def __subclasshook__(cls, subclass):
        return (
            hasattr(subclass, "init_client")
            and callable(subclass.init_client)
            and hasattr(subclass, "search_recent_tweets_with_stats")
            and callable(subclass.search_recent_tweets_with_stats)
            or NotImplemented
        )

    @abc.abstractmethod
    def init_client(
        self: "InterfaceTwitterAPIv2",
        bearer_token: str,
    ):
        """Init twitter V2 API client"""
        raise NotImplementedError

    @abc.abstractmethod
    def search_recent_tweets_with_stats(
        self: "InterfaceTwitterAPIv2", search_request: str
    ):
        """Returns tweets"""
        raise NotImplementedError

class PersistenceAdapter:
    """Interface for persisting data"""

    @classmethod
    def __subclasshook__(cls, subclass):
        return (
            hasattr(subclass, "init_db")
            and callable(subclass.init_db)
            and hasattr(subclass, "update_db")
            and callable(subclass.update_db)
            and hasattr(subclass, "persist_old_tweet_data_removal")
            and callable(subclass.persist_old_tweet_data_removal)
            or NotImplemented
        )

    def init_db(self, db_json_file):
        """Init file db: very specific... :-<"""
        raise NotImplementedError

    def update_db(
        self,
        indices: List[int64],
        rows: List[Tuple[datetime, str, int, int, int, bool, str, int]],
    ):
        """update_db with new tweet data"""
        raise NotImplementedError

    def persist_old_tweet_data_removal(self, old_date_limite: str):
        """Purge old tweets"""
        raise NotImplementedError

class TimerAdapter:
    """Interface for singaling with timer"""

    @classmethod
    def __subclasshook__(cls, subclass):
        return (
            hasattr(subclass, "signal_next_update")
            and callable(subclass.signal_next_update)
            or NotImplemented
        )

    def signal_next_update(self, update_interval = 86400):
        """when to signal next"""
        raise NotImplementedError
