from dataclasses import dataclass, field
from datetime import datetime
import os
from typing import List, Optional, Tuple
from appracadabra.adapters.interface_adapters import PersistenceAdapter, TimerAdapter
from numpy import int64
import pandas  # type: ignore
from appracadabra.domain_logic.models import TweetData

default_columns = [
    "created_at",
    "text",
    "reply_count",
    "like_count",
    "retweet_count",
    "references_other_tweet",
    "media_key",
    "video_view_count",
]


def remove_old_tweet_data(dataframe: pandas.DataFrame, old_date_limite: str):
    created_at_col_name = "created_at"
    print(dataframe.shape)
    nb_rows_before = dataframe.shape[0]
    dataframe = dataframe[
        (
            pandas.to_datetime(dataframe[created_at_col_name], utc=True)
            >= pandas.to_datetime(old_date_limite, utc=True)
        )
    ]
    nb_rows_after = dataframe.shape[0]
    print(dataframe.shape)
    print(
        f"{nb_rows_before - nb_rows_after} rows have been deleted during old tweets removal"
    )
    return dataframe


@dataclass
class PandasPersistenceAdapter(PersistenceAdapter):

    m_db: pandas.DataFrame = field(
        default_factory=lambda: pandas.DataFrame(columns=default_columns)
    )
    db_file: Optional[str] = field(default=None)

    def init_db(self, db_json_file):
        self.db_file = db_json_file
        if os.path.exists(db_json_file):
            if self.m_db.empty:
                # If db empty
                df = pandas.read_csv(db_json_file, index_col=0)
                self.m_db = df

    def get_tweet_date_creation(self):
        return self.m_db[default_columns[0]]

    def update_db(
        self,
        indices: List[int64],
        rows: List[Tuple[datetime, str, int, int, int, bool, str, int]],
    ):
        if not rows:
            # rows empty, nothing to do
            return

        new_tweet_data = pandas.DataFrame(
            rows,
            columns=default_columns,
            index=indices,
        )

        # Remove tweets already persisted, so they can be updated (likes, ...)
        m_db_without_new_ones_identical_keys = self.m_db[
            ~self.m_db.index.isin(new_tweet_data.index)
        ]
        self.m_db = pandas.concat(
            (m_db_without_new_ones_identical_keys, new_tweet_data)
            # (self.m_db, new_ones)
        )
        self.m_db.to_csv(self.db_file)

    def persist_old_tweet_data_removal(self, old_date_limite: str):
        self.m_db = remove_old_tweet_data(self.m_db, old_date_limite=old_date_limite)
        self.m_db.to_csv(self.db_file)

class DummyTimer(TimerAdapter):
    """Some timer implementation"""

    def signal_next_update(self, update_interval = 86400):
        """when to signal next"""
        print(f"Normally I, DummyTimer, would signal tweet ingestion in {update_interval} seconds")

